fetch('http://localhost/CatDogAPI/api/index.php?animal=dogs', {
    method: 'GET',
    headers: {
        'Authorization': 'Bearer 93407dce085f309a1308c8999d577e1f4b5c10f9b3076eb67f03c7616f2afd9c'
    }
})
    .then(function(response) {
        if (!response.ok) {
            throw new Error('Network response was not OK. Status: ' + response.status);
        }
        return response.json(); // Parse the response as JSON
    })
    .then(jsonData => {
        console.log(jsonData);

        let img = document.getElementById('image-container');
        img.src = jsonData['image_url'];
    })
    .catch(error => {
        console.error('Fetch Error:', error);
    });


// fetch('http://localhost/CatDogAPI/api/index.php?animal=dogs', {
//     method: 'GET',
//     headers: {
//         'Authorization': 'Bearer 61fa31d1a584fb41b8b30ec5669a62ebcf77d114bba83b5684eccadad0ebbcb2'
//     }
// })
//     .then(function(response) {
//         response.json().then(jsonData => {
//             console.log(jsonData);
//
//             let img = document.getElementById('image-container');
//             img.src = jsonData['image_url']
//
//         });
//     })



