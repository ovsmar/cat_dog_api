<?php
include "../debug/debug.php";

// Function to generate a random token
function generateToken() {
    return bin2hex(random_bytes(32)); // 32 bytes (256 bits) token
}

// Function to validate a token
function validateToken($token, $pdo) {
    // Create an SQL query to check if the token is valid
    $sql = "SELECT user_id, token FROM users WHERE token = :token";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':token', $token, PDO::PARAM_STR);

    if ($stmt->execute()) {
        // Fetch the user data
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            // Token is valid
            return [
                'valid' => true,
                'user_id' => $user['user_id']
            ];
        }
    }

    // Token is not valid
    return ['valid' => false];
}


// Function to validate a token
function validateTokenSession($token, $userId, $pdo) {

    // Debugging: Log the token and user ID
//    var_dump("Token: $token, UserID: $userId");

    // Create an SQL query to check if the token is valid
    $sql = "SELECT token FROM users WHERE user_id = :userId";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
//    var_dump($stmt);

    if ($stmt->execute()) {
        // Check if a row was returned, indicating a valid token
        $rowCount = $stmt->rowCount();

        // Debugging: Log the token from the database and the token from the request
        if ($rowCount > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
//            var_dump("Token from DB: " . $row['token']); // Debug line
//            var_dump("Token from Request: $token"); // Debug line
        }


        return $rowCount > 0;
    }

    return false;
}







