<?php
global $pdo;
include '../includes/db.php'; // Include the database connection script
include "../debug/debug.php";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Get user input from the registration form
    $username = $_POST["username"];
    $password = password_hash($_POST["password"], PASSWORD_BCRYPT); // Hash the password for security

    // Check if the username already exists
    $checkSql = "SELECT username FROM users WHERE username = :username";
    $checkStmt = $pdo->prepare($checkSql);
    $checkStmt->bindParam(':username', $username, PDO::PARAM_STR);
    $checkStmt->execute();

    if ($checkStmt->fetchColumn()) {
        $message = "Username already exists. Please choose a different one.";
    } else {
        // Proceed with the registration
        $sql = "INSERT INTO users (username, password_hash, token, request_limit) VALUES (:username, :password, '', 100)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $message = "Registration successful! Login to the system to get your API token.";
        } else {
            // Handle database errors
            $message = "Error: Registration failed.";
        }
    }

    // Redirect back to the registration page with the message as a URL parameter
    header("Location: ../public/register.php?message=" . urlencode($message));
    exit();
}

// No need to close the database connection explicitly when using PDO

