<?php
include '../includes/db.php'; // Include the database connection script
include "../debug/debug.php";

// get admin inforamtions
function selectAdminName($name)
{
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM users WHERE username = ? ');
    $req->execute([$name]);
    return $req->fetch();
}

// get all users
function getUsers()
{
    global $pdo;
    return $pdo->query('SELECT * FROM users')->fetchAll();
}

// delete user
function processUserDeletion(): void
{

    function deleteUser($userId): void
    {
        global $pdo;
        $req = $pdo->prepare('DELETE FROM users WHERE user_id = ?');
        $req->execute([$userId]);
    }

    if (isset($_POST['delete']) && $_POST['delete'] !== "") {
        $supp = "delete ";
        $garde = $_POST['delete'];
        $id = str_replace($supp, '', $garde);

        $id = (int)$id;

        deleteUser($id);

        header('Location: ../dashboards/AdminDashboard.php');
        exit();
    }
}

// update role
function processRoleUpdation(): void
{
    global $pdo;

    if (isset($_POST['updateRole']) && !empty($_POST['updateRole'])) {
        $userId = $_POST['id'];
        $newRole = $_POST['updateRole'];


        $userId = (int)$userId;

        $sql = "UPDATE users SET role = :role WHERE user_id = :userId";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':role', $newRole, PDO::PARAM_STR);
        $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);

        if ($stmt->execute()) {
            header('Location: ../dashboards/AdminDashboard.php');
        } else {
            echo "Role update failed.";
        }
        exit();
    }
}


// Call the function to process user deletion
processUserDeletion();
processRoleUpdation();
