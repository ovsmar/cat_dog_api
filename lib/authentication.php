<?php

function getUserIdFromAuthentication($username) {
    global $pdo; // Use your database connection here
    $sql = "SELECT user_id FROM users WHERE username = :username";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':username', $username, PDO::PARAM_STR);

    if ($stmt->execute() && $stmt->rowCount() === 1) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['user_id'];
    }

    return null; // User not found
}
