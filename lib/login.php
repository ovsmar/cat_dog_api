<?php
global $pdo;
include '../includes/db.php'; // Include the database connection script
include '../lib/token.php'; // Include the token-related functions
include "../debug/debug.php";

session_start(); // Start the session

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Get user input from the login form
    $username = $_POST["username"];
    $password = $_POST["password"];

    // Replace this part with your authentication logic
    $sql = "SELECT user_id, password_hash, token, username, role, last_reset_date FROM users WHERE username = :username";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':username', $username, PDO::PARAM_STR);

    if ($stmt->execute() && $stmt->rowCount() == 1) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $hashed_password = $row["password_hash"];
        $userId = $row["user_id"];
        $token = $row["token"];

        // Fetch the role and last reset date from the database
        $userRole = $row["role"]; // Assuming 'role' is the name of the column in your 'users' table
        $lastResetDate = $row["last_reset_date"]; // Assuming 'last_reset_date' is the name of the column

        // Calculate the time difference between the last reset date and the current date
        $currentTime = time();
        $lastResetTimestamp = strtotime($lastResetDate);
        $timeDiff = $currentTime - $lastResetTimestamp;
        $resetInterval = 30 * 24 * 3600; // 30 days in seconds

//        echo "Current Time: $currentTime<br>";
//        echo "Last Reset Timestamp: $lastResetTimestamp<br>";
//        echo "Time Difference: $timeDiff<br>";

        if (password_verify($password, $hashed_password)) {
            if (empty($token) || $timeDiff >= $resetInterval) {

                // If the user doesn't have a token or it's been more than a month, reset the request limit
                $token = generateToken();
                $sql = "UPDATE users SET token = :token, request_limit = 100, last_reset_date = NOW() WHERE user_id = :userId";
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':token', $token, PDO::PARAM_STR);
                $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
                if ($stmt->execute()) {
                    // Update the last reset date to the current date
                    $lastResetTimestamp = $currentTime;
                } else {
                    echo "Error storing token in the database.";
                    exit();
                }
            }

            // Fetch the role from the database
            $userRole = $row["role"]; // Assuming 'role' is the name of the column in your 'users' table

            // After a successful login, set a session with the username and role
            $_SESSION['username'] = $row['username'];
            $_SESSION['role'] = $userRole; // Set the user's role based on the database

            // Determine the role and redirect accordingly
            if ($_SESSION['role'] === 'admin') {
                header('Location: ../dashboards/AdminDashboard.php');
                exit();
            } elseif ($_SESSION['role'] === 'user') {
                header('Location: ../dashboards/UserDashboard.php');
                exit();
            } else {
                $message = "Unknown user role"; // Handle any other roles as needed
            }
        } else {
            $message = "Invalid username or password.";
        }
    } else {
        $message = "User not found."; // Handle the case where the user doesn't exist.
    }

        header('Location: ../public/login.php?message=' . urlencode($message));
        exit();

}
