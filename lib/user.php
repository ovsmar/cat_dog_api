<?php
include '../includes/db.php'; // Include the database connection script
include "../debug/debug.php";
function selectUserName($name){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM users WHERE username = ? ');
    $req->execute([$name]);
    return $req->fetch(); // Use fetch() to retrieve a single row
}

