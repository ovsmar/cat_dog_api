<?php

include '../lib/user.php'; // Include your user-related functions
include '../debug/debug.php'; // Include debugging tools
include '../protected/protected.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../public/css/dashboard.css">

    <style>

    </style>
</head>
<body>
<div class="dashboard">
    <div class="header">User Dashboard</div>
    <?php
    $user = selectUserName($_SESSION['username']); // Fetch user information based on the username
    if ($user !== false) {

        $lastResetDate = strtotime($user['last_reset_date']);
        $resetInterval = 30 * 24 * 3600; // 30 days in seconds
        $currentTime = time();
        $timeDiff = $currentTime - $lastResetDate;
        $daysLeft = floor(($resetInterval - $timeDiff) / (60 * 60 * 24)); // Calculate days left


        ?>
        <div class="info">
            <p><strong>Username:</strong> <?php echo $user['username']; ?></p>
            <p><strong>Role:</strong> <?php echo $user['role']; ?></p>
            <p><strong>Token:</strong> <?php echo $user['token']; ?></p>
            <p><strong>Request Limit:</strong> <?php echo $user['request_limit']; ?></p>
            <p><strong>Created at:</strong> <?php echo $user['created_at']; ?></p>
            <p><strong>Days Until Reset:</strong> <?php echo $daysLeft; ?></p>
            <p><a href="../sessiondelete/sessiondelete.php" style="color:red">EXIT </a>
                <!-- Add additional information as needed -->
        </div>

        <?php
    } else {
        echo "User not found."; // Handle the case where the user information is not available.
    }
    ?>
</div>
</body>
</html>
