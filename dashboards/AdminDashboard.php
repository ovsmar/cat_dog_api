<?php

include '../lib/admin.php';
include '../debug/debug.php';
include '../protected/protected.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../public/css/dashboard.css">

    <style>

    </style>
</head>
<body>
<div class="dashboard">
    <div class="header">Admin Dashboard</div>
    <?php
    $user = selectAdminName($_SESSION['username']); // Fetch user information based on the username
    if ($user !== false) {
        // Display user information
        ?>
        <div class="info">
            <p><strong>Username:</strong> <?php echo $user['username']; ?></p>
            <p><strong>Role:</strong> <?php echo $user['role']; ?></p>
            <p><strong>Token:</strong> <?php echo $user['token']; ?></p>
            <p><strong>Request Limit:</strong> <?php echo $user['request_limit']; ?></p>
            <p><strong>Created at:</strong> <?php echo $user['created_at']; ?></p>
            <p><a href="../sessiondelete/sessiondelete.php" style="color:red">EXIT </a>
        </div>
        <?php
    } else {
        echo "User not found."; // Handle the case where the user information is not available.
    }
    ?>
    <table>
        <thead>
        <tr>
            <th>User ID</th>
            <th>Username</th>
            <th colspan="2">Role</th>
            <th>Token</th>
            <th>Request limit (month)</th>
            <th>Last request date</th>
            <th>Days Left Until Reset</th>
            <th>Created at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <!-- Loop through user data and populate the table -->
        <?php foreach (getUsers() as $user): ?>
            <tr>
                <td><?php echo $user['user_id']; ?></td>
                <td><?php echo $user['username']; ?></td>


                <form action="../lib/admin.php" method="post">
                    <td class="<?php echo $user['role'] === 'admin' ? 'admin-td' : ''; ?>"><textarea class="textarea"
                                                                                                     name="updateRole"
                                                                                                     value=""><?php echo $user['role']; ?></textarea>
                    </td>
                    <input type="hidden" name="id" value="<?php echo $user['user_id']; ?>">
                    <td><input type="submit" class="edit-button" value="update"/></td>
                </form>

                <td>  <?php
                    if (empty($user['token'])) {
                        echo '<span style="color:red">User/Admin doesn\'t have an active account yet.</span>';
                    } else {
                        echo '<span style="color:green">' . $user['token'] . '</span>';
                    }
                    ?></td>
                <td><?php echo $user['request_limit']; ?></td>
                <td><?php echo $user['last_reset_date']; ?></td>
                <td>
                    <?php
                    $lastResetDate = strtotime($user['last_reset_date']);
                    $resetInterval = 30 * 24 * 3600; // 30 days in seconds
                    $currentTime = time();
                    $timeDiff = $currentTime - $lastResetDate;
                    $daysLeft = floor(($resetInterval - $timeDiff) / (60 * 60 * 24)); // Calculate days left
                    echo $daysLeft;
                    ?>
                </td>
                <td><?php echo $user['created_at']; ?></td>
                <td>
                    <div class="actions">
                        <!--                    <button class="edit-button">Edit</button>-->
                        <button type="button" class="delete-button"
                                onclick="confirmDelete(<?php echo $user['user_id']; ?>)">Delete
                        </button>
                        <form id="delete-form-<?php echo $user['user_id']; ?>" action="../lib/admin.php" method="post">
                            <input type="hidden" name="delete" value="delete <?php echo $user['user_id']; ?>">
                        </form>

                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>
<script src="../public/js/dashboard.js"></script>
</body>
</html>



