<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <title>User Login</title>
</head>
<body>
<div class="login-container">
    <h2>User Login</h2>
    <?php
    if (isset($_GET['message'])) {
        echo '<p class="message">' . $_GET['message'] . '</p>';
    }
    ?>
    <form class="login-form" action="../lib/login.php" method="post">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" placeholder="Username" required>

        <label for="password">Password:</label>
        <input type="password" name="password" id="password" placeholder="Password" required>

        <input type="submit" value="Login">
    </form>
</div>
</body>
</html>