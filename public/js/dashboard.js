function confirmDelete(userId) {
    if (confirm("Are you sure you want to delete this user?")) {
        // If the user confirms, submit the form
        document.getElementById('delete-form-' + userId).submit();
    }
}


//textarea auto-resize
const tx = document.getElementsByTagName("textarea");
for (let i = 0; i < tx.length; i++) {
    tx[i].setAttribute("style", "height:" + (tx[i].scrollHeight) + "px;overflow-y:hidden;");
    tx[i].addEventListener("input", OnInput, false);
}

function OnInput() {
    this.style.height = 0;
    this.style.height = (this.scrollHeight) + "px";
}
