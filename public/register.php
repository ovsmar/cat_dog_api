<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/register.css">
    <title>User Registration</title>
</head>
<body>
<div class="registration-container">
    <h2>User Registration</h2>
    <?php
    if (isset($_GET['message'])) {
        echo '<p class="message">' . $_GET['message'] . '</p>';
    }
    ?>
    <form class="registration-form" action="../lib/register.php" method="post">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" placeholder="Username" required>

        <label for="password">Password:</label>
        <input type="password" name="password" id="password" placeholder="Password" required>

        <input type="submit" value="Register">
    </form>
</div>
</body>
</html>