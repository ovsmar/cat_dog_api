<?php
session_start();

if (!isset($_SESSION['role']) || ($_SESSION['role'] !== 'user' && $_SESSION['role'] !== 'admin')) {
    // If not authenticated or role is not 'user' or 'admin', redirect to the login page
    header('Location: ../error/error.php');
    exit();
}