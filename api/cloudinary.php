<?php
require '../vendor/autoload.php';
include "../debug/debug.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

use Cloudinary\Configuration\Configuration;
use Cloudinary\Cloudinary;

//$resp = $cloudinary->uploadApi()->upload( '1.jpg', ["use_filename" => TRUE, "unique_filename" => FALSE, "folder" => "cats"] );

class ImageProvider
{
    private $cloudinary;

    public function __construct()
    {
        $config = Configuration::instance();
        $config->cloud->cloudName = $_ENV['cloudName'];
        $config->cloud->apiKey = $_ENV['apiKey'];
        $config->cloud->apiSecret = $_ENV['apiSecret'];
        $config->url->secure = true;

        $this->cloudinary = new Cloudinary($config);
    }

    public function getRandomCatImage()
    {
        $cats = $this->cloudinary->searchApi()
            ->expression('cats')
            ->withField('image_metadata')
            ->withField('tags')
            ->execute();

        if ($cats && isset($cats['resources']) && count($cats['resources']) > 0) {
            $randomIndex = array_rand($cats['resources']);
            $randomImageUrl = $cats['resources'][$randomIndex]['url'];

            return $randomImageUrl;

        }

        return null;
    }

    public function getRandomDogImage()
    {
        $dogs = $this->cloudinary->searchApi()
            ->expression('dogs')
            ->withField('image_metadata')
            ->withField('tags')
            ->execute();

        if ($dogs && isset($dogs['resources']) && count($dogs['resources']) > 0) {
            $randomIndex = array_rand($dogs['resources']);
            $randomImageUrl = $dogs['resources'][$randomIndex]['url'];

            return $randomImageUrl;
        }
        return null;
    }
}

























//if ($cats && isset($cats['resources']) && count($cats['resources']) > 0) {
//    // Select a random index from the resources array
//    $randomIndex = array_rand($cats['resources']);
//
//    // Get the URL of the randomly selected resource
//    $randomImageUrl = $cats['resources'][$randomIndex]['url'];
//
//    // Create a JSON response with the random URL
//    $response = [
//        'random_image_url' => $randomImageUrl,
//    ];
//
//    // Set the content type to JSON
//    header('Content-Type: application/json');
//
//    // Print the JSON response
//    echo json_encode($response);
//} else {
//    // Handle the case where no cat images were found
//    $response = [
//        'error' => 'No cat images found.',
//    ];
//
//    // Set the content type to JSON
//    header('Content-Type: application/json');
//
//    // Print the JSON response
//    echo json_encode($response);
//}