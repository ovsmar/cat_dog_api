<?php
// Include necessary files and start the session
global $pdo;
include '../includes/db.php';
include '../lib/token.php';
include "../debug/debug.php";
include '../lib/authentication.php';
include "cloudinary.php";
$provider = new ImageProvider();
$randomCatImageUrl = $provider->getRandomCatImage();
$randomDogImageUrl = $provider->getRandomDogImage();
session_start();

// Function to check and update request limit for a user
function checkAndUpdateRequestLimit($userId, $pdo)
{
    $response = [];

    // Select the request limit and the last request timestamp for the user
    $sql = "SELECT request_limit, last_request_timestamp FROM users WHERE user_id = :userId";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);

    if ($stmt->execute()) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $requestLimit = $row['request_limit'];
        $lastRequestTimestamp = strtotime($row['last_request_timestamp']);
        $currentTimestamp = time();
        $timeElapsed = $currentTimestamp - $lastRequestTimestamp;

        if ($timeElapsed >= 1) { // Only allow 1 request per second
            if ($requestLimit > 0) {
                $requestLimit--;

                // Update the request limit and last request timestamp in the database
                $updateSql = "UPDATE users SET request_limit = :requestLimit, last_request_timestamp = NOW() WHERE user_id = :userId";
                $updateStmt = $pdo->prepare($updateSql);
                $updateStmt->bindParam(':requestLimit', $requestLimit, PDO::PARAM_INT);
                $updateStmt->bindParam(':userId', $userId, PDO::PARAM_INT);

                if ($updateStmt->execute()) {
                    return $requestLimit;
                } else {
                    $response['error'] = 'Failed to update request limit in the database.';
                }
            } else {
                $response['error'] = 'Request limit exceeded for the user.';
            }
        } else {
            $response['error'] = 'Rate limit exceeded. Please wait before making another request.';
        }
    } else {
        $response['error'] = 'Failed to retrieve request limit and last request timestamp from the database.';
    }

    return $response;
}


$headers = getallheaders();
$apiToken = isset($headers['Authorization']) ? str_replace('Bearer ', '', $headers['Authorization']) : '';
$response = [];

if (!empty($apiToken)) {
    // Validate the API token
    $tokenValidationResult = validateToken($apiToken, $pdo);

    if ($tokenValidationResult['valid']) {
        $userId = $tokenValidationResult['user_id'];
        $requestLimit = checkAndUpdateRequestLimit($userId, $pdo);

        if (!isset($requestLimit['error'])) {
            $animalType = isset($_GET['animal']) ? $_GET['animal'] : '';

            if ($animalType === 'cats' || $animalType === 'dogs') {
                if ($animalType === 'cats') {
                    $randomImageUrl = $randomCatImageUrl;
                } else {
                    $randomImageUrl = $randomDogImageUrl;
                }

                $response['animal'] = $animalType;
                $response['image_url'] = $randomImageUrl;
                $response['requestLimit'] = $requestLimit;
            } else {
                $response['error'] = 'Invalid animal type. Use "cats" or "dogs".';
            }
        } else {
            $response = $requestLimit;
        }
    } else {
        $response['error'] = 'Invalid API token. Access denied.';
    }
} elseif (isset($_SESSION['username'])) {
    // Get the API token from the request headers
    $apiToken = isset($_SERVER['HTTP_AUTHORIZATION']) ? str_replace('Bearer ', '', $_SERVER['HTTP_AUTHORIZATION']) : '';
    $username = $_SESSION['username'];
    $userId = getUserIdFromAuthentication($username);

    if (validateTokenSession($apiToken, $userId, $pdo)) {
        $requestLimit = checkAndUpdateRequestLimit($userId, $pdo);

        if (!isset($requestLimit['error'])) {
            $animalType = isset($_GET['animal']) ? $_GET['animal'] : '';
            $response = [];

            if ($animalType === 'cats' || $animalType === 'dogs') {
                if ($animalType === 'cats') {
                    $randomImageUrl = $randomCatImageUrl;
                } else {
                    $randomImageUrl = $randomDogImageUrl;
                }

                $response['animal'] = $animalType;
                $response['image_url'] = $randomImageUrl;
                $response['requestLimit'] = $requestLimit;
            } else {
                $response['error'] = 'Invalid animal type. Use "cats" or "dogs".';
            }
        } else {
            $response = $requestLimit;
        }
    } else {
        $response['error'] = 'Invalid API token. Access denied.';
    }
} else {
    $response['error'] = 'User not logged in.';
}

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, OPTIONS');
header('Access-Control-Allow-Headers: Authorization');

echo json_encode($response, JSON_UNESCAPED_SLASHES);
